# sklearn-mooc-extract


The MOOC is hosted on the [FUN-MOOC](https://fun-mooc.fr/) platform which
is free and does not use the student data for any other purpose than improving
the educational material.

The static version of the course can be browsed online: https://inria.github.io/scikit-learn-mooc

## Course description

The course description can be found here:
https://inria.github.io/scikit-learn-mooc/index.html
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:93d6316cfaa44d42ad2731bc8ae5a421?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)


# Install
The course uses Python 3 and some data analysis packages such as Numpy, Pandas,
scikit-learn, and matplotlib.

## Install Miniconda

**This step is only necessary if you don't have conda installed already**:

- download the Miniconda installer for your operating system (Windows, MacOSX
  or Linux) [here](https://docs.conda.io/en/latest/miniconda.html)
- run the installer following the instructions
  [here](https://conda.io/projects/conda/en/latest/user-guide/install/index.html#regular-installation)
  depending on your operating system.

## Create conda environment

```sh
conda env create -f environment.yml
```


